package app;

import java.awt.Color;
import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.LayoutStyle.ComponentPlacement;
import javax.swing.SwingConstants;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;

public class MainWindow {

	private JFrame frmDbmmLab;
	/**
	 * @wbp.nonvisual location=523,71
	 */
	private final ButtonGroup buttonGroup = new ButtonGroup();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainWindow window = new MainWindow();
					window.frmDbmmLab.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public MainWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmDbmmLab = new JFrame();
		frmDbmmLab.setTitle("DBMM - Lab1 - MPEG7");
		frmDbmmLab.setResizable(false);
		frmDbmmLab.setBounds(100, 100, 873, 706);
		frmDbmmLab.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmDbmmLab.getContentPane().setLayout(null);
		
		JPanel panel_query = new JPanel();
		panel_query.setBorder(new TitledBorder(null, "Query:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_query.setBounds(10, 6, 857, 308);
		frmDbmmLab.getContentPane().add(panel_query);
		panel_query.setLayout(null);
		
		JPanel panel_img = new JPanel();
		panel_img.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Image:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_img.setBounds(6, 23, 185, 235);
		panel_query.add(panel_img);
		
		JLabel lblQueryImage = new JLabel("Select a query image");
		lblQueryImage.setBackground(new Color(105, 105, 105));
		lblQueryImage.setHorizontalAlignment(SwingConstants.CENTER);
		lblQueryImage.setIcon(null);
		
		JButton btnSelect = new JButton("Select...");
		btnSelect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		GroupLayout gl_panel_img = new GroupLayout(panel_img);
		gl_panel_img.setHorizontalGroup(
			gl_panel_img.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_img.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblQueryImage, GroupLayout.DEFAULT_SIZE, 161, Short.MAX_VALUE)
					.addContainerGap())
				.addGroup(Alignment.TRAILING, gl_panel_img.createSequentialGroup()
					.addGap(43)
					.addComponent(btnSelect, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addGap(37))
		);
		gl_panel_img.setVerticalGroup(
			gl_panel_img.createParallelGroup(Alignment.LEADING)
				.addGroup(gl_panel_img.createSequentialGroup()
					.addContainerGap()
					.addComponent(lblQueryImage, GroupLayout.PREFERRED_SIZE, 168, GroupLayout.PREFERRED_SIZE)
					.addPreferredGap(ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
					.addComponent(btnSelect))
		);
		panel_img.setLayout(gl_panel_img);
		
		JPanel panel_weights = new JPanel();
		panel_weights.setBounds(571, 23, 280, 235);
		panel_query.add(panel_weights);
		panel_weights.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Weights (LIRE Built-In Only):", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_weights.setLayout(new GridLayout(3, 2, 0, 0));
		
		JLabel lblCLD = new JLabel("Color Layout:");
		panel_weights.add(lblCLD);
		
		JSlider cldSlider = new JSlider();
		cldSlider.setValue(100);
		cldSlider.setPaintTicks(true);
		cldSlider.setMajorTickSpacing(25);
		panel_weights.add(cldSlider);
		
		JLabel labelSCD = new JLabel("Scalable Color:");
		panel_weights.add(labelSCD);
		
		JSlider scdSlider = new JSlider();
		scdSlider.setValue(100);
		scdSlider.setPaintTicks(true);
		scdSlider.setMajorTickSpacing(25);
		panel_weights.add(scdSlider);
		
		JLabel labelEHD = new JLabel("Edge Histogram:");
		panel_weights.add(labelEHD);
		
		JSlider ehdSlider = new JSlider();
		ehdSlider.setValue(100);
		panel_weights.add(ehdSlider);
		ehdSlider.setPaintTicks(true);
		ehdSlider.setMajorTickSpacing(25);
		
		JPanel panel_mergingSchemes = new JPanel();
		panel_mergingSchemes.setBounds(367, 23, 192, 235);
		panel_query.add(panel_mergingSchemes);
		panel_mergingSchemes.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Merging Schemes:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_mergingSchemes.setLayout(new GridLayout(4, 0, 0, 10));
		
		JRadioButton rdbtnLireBuiltin = new JRadioButton("LIRE Built-In");
		rdbtnLireBuiltin.setSelected(true);
		panel_mergingSchemes.add(rdbtnLireBuiltin);
		buttonGroup.add(rdbtnLireBuiltin);
		
		JRadioButton rdbtnBordaCount = new JRadioButton("Borda Count");
		panel_mergingSchemes.add(rdbtnBordaCount);
		buttonGroup.add(rdbtnBordaCount);
		
		JRadioButton rdbtnRankProduct = new JRadioButton("Rank Product");
		panel_mergingSchemes.add(rdbtnRankProduct);
		buttonGroup.add(rdbtnRankProduct);
		
		JRadioButton rdbtnInvertedRankPosition = new JRadioButton("Inverted Rank Position");
		panel_mergingSchemes.add(rdbtnInvertedRankPosition);
		buttonGroup.add(rdbtnInvertedRankPosition);
		
		JPanel panel_descriptors = new JPanel();
		panel_descriptors.setBounds(203, 23, 152, 235);
		panel_query.add(panel_descriptors);
		panel_descriptors.setBorder(new TitledBorder(null, "Descriptors:", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_descriptors.setLayout(new GridLayout(3, 0, 0, 0));
		
		JCheckBox chckbxColorLayout = new JCheckBox("Color Layout");
		chckbxColorLayout.setSelected(true);
		panel_descriptors.add(chckbxColorLayout);
		
		JCheckBox chckbxScalableColor = new JCheckBox("Scalable Color");
		chckbxScalableColor.setSelected(true);
		panel_descriptors.add(chckbxScalableColor);
		
		JCheckBox chckbxEdgeHistogram = new JCheckBox("Edge Histogram");
		chckbxEdgeHistogram.setSelected(true);
		panel_descriptors.add(chckbxEdgeHistogram);
		
		JButton btnSearch = new JButton("Search!");
		btnSearch.setBounds(734, 270, 117, 29);
		panel_query.add(btnSearch);
		
		JPanel panel_results = new JPanel();
		panel_results.setBorder(new TitledBorder(new EtchedBorder(EtchedBorder.LOWERED, null, null), "Results:", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(0, 0, 0)));
		panel_results.setLayout(null);
		panel_results.setBounds(10, 326, 857, 352);
		frmDbmmLab.getContentPane().add(panel_results);
		
		JScrollPane resultsScrollPane = new JScrollPane();
		resultsScrollPane.setBounds(6, 25, 845, 321);
		panel_results.add(resultsScrollPane);
	}
}
