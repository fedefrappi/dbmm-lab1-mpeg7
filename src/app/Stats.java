package app;

import java.util.List;

public class Stats {
	private List<SearchResult> searchResults;
	private List<String> groundtruthResults;
	
	public Stats(List<SearchResult> searchResults, List<String> groundtruthResults) {
		super();
		this.searchResults = searchResults;
		this.groundtruthResults = groundtruthResults;
	}
	
	public double getPrecision(int k){
		int num = 0;
		List<SearchResult> subSearchResults = searchResults.subList(0, k);
//		System.out.println("k="+k);
//		System.out.println(subSearchResults);
		for(SearchResult searchResult : subSearchResults){
			for(String groundtruthResult : groundtruthResults){
				if(searchResult.getResult().equalsIgnoreCase(groundtruthResult)){
					num++;
				}
			}
		}
		return (double)num/(double)k;
	}
	
	public double getRecall(int k){
		int num = 0;
		List<SearchResult> subSearchResults = searchResults.subList(0, k);
		for(SearchResult searchResult : subSearchResults){
			for(String groundtruthResult : groundtruthResults){
				if(searchResult.getResult().equalsIgnoreCase(groundtruthResult)){
					num++;
				}
			}
		}
		return (double)num/(double)groundtruthResults.size();	
	}
	
	public double getAveragePrecision(int maxK){
		double num = 0;
		for(int k=0; k<maxK; k++){
			num += getPrecision(k+1)*(groundtruthResults.indexOf(searchResults.get(k).getResult())==-1?0d:1d);
		}
		return num/(double)(groundtruthResults.size());
	}
	
}
