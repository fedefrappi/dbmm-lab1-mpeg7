package app;

public class SearchResult implements Comparable<SearchResult>{
	private String query;
	private String result;
	private int position;
	private double similarity;
	
	public SearchResult(String query, String result, int position,
			double similarity) {
		super();
		this.query = query;
		this.result = result;
		this.position = position;
		this.similarity = similarity;
	}

	public String getQuery() {
		return query;
	}

	public String getResult() {
		return result;
	}

	public int getPosition() {
		return position;
	}

	public double getSimilarity() {
		return similarity;
	}

	@Override
	public int compareTo(SearchResult sr) {
		if(this.similarity < sr.getSimilarity()){
			return 1;
		}else if(this.similarity > sr.getSimilarity()){
			return -1;
		}else{
			return 0;
		}
	}
	
	@Override
	public String toString(){
		return this.result + " " + this.similarity;
	}
}
