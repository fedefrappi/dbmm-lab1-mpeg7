package app;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

public class GTFParser {
	public Map<String, List<String>> parseAndFilterGroundTruthFile(int minNumberOfRelevantResults){
		Map<String, List<String>> result = new HashMap<String, List<String>>();
		try {
			BufferedReader bReader = new BufferedReader(new FileReader("ucid.v2.groundtruth.txt"));
			String line;
			String currentQuery = new String();
			List<String> currentQueryTargets = null;
			while((line=bReader.readLine())!=null){
				if (!line.startsWith("#") && line.length()>0) {
					if (!line.startsWith(" ") && line.trim()!="") {
						currentQuery = line.substring(0, line.length()-1);
						//System.out.println("query: " + currentQuery);
						currentQueryTargets = new ArrayList<String>();
						result.put(currentQuery, currentQueryTargets);
					} else {
						//System.out.println("\t target:" + line);
						currentQueryTargets.add(line.trim());
					}
				}
			}
			bReader.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		List<String> toBeDeleted = new ArrayList<String>();
		for(Entry<String, List<String>> entry : result.entrySet()){
			if(entry.getValue().size()<minNumberOfRelevantResults){
				toBeDeleted.add(entry.getKey());
			}
		}
		for(String key : toBeDeleted){
			result.remove(key);
		}
		return result;
	}
}
